README

Assignment 1 for CPE 123 is a soundboard-esque creation made by 
Thomas Franklin and Chan Le. 

==INSTALLATION==
Copy the folder titled Assignment onto the desktop.

Change \\Users\\Owner in the definitions section into whatever your profile is instead. [i.e. \\Users\\(YourProfileHere)\\]

Run the program and Enjoy!

==HOW TO UNINSTALL==
Delete the folder from the desktop and delete zip file titled Assignment 1.